FLAGS := -c -Wall -pedantic

solver.out: main.o vector3d.o matrix3x3.o setOfEquations.o
	g++ main.o vector3d.o matrix3x3.o setOfEquations.o -o solver
	rm *.o
	./solver < sets/set0.dat

main.o: src/main.cpp inc/matrix3x3.hh inc/vector3d.hh
	g++ $(FLAGS) src/main.cpp

setOfEquations.o : src/setOfEquations.cpp inc/setOfEquations.hh inc/matrix3x3.hh inc/vector3d.hh
	g++ $(FLAGS) src/setOfEquations.cpp

matrix3x3.o: src/matrix3x3.cpp inc/matrix3x3.hh inc/vector3d.hh
	g++ $(FLAGS) src/matrix3x3.cpp

vector3d.o: src/vector3d.cpp inc/vector3d.hh
	g++ $(FLAGS) src/vector3d.cpp