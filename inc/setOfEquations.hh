#pragma once

#include "matrix3x3.hh"
#include "vector3d.hh"

#define ERR_READING_SET -3
#define ERR_DET_0 -4

using std::cerr;
using std::cout;
using std::endl;

class SetOfEquations{
private:
    Matrix3x3 _matrix;
    Vector3d _absoluteTerm;

public:
    SetOfEquations(Matrix3x3 matrix, Vector3d vector) : _matrix(matrix), _absoluteTerm(vector) {};
    SetOfEquations() : SetOfEquations(Matrix3x3(), Vector3d()) {};

    Vector3d solve() const;

    void showPrettySolution(Vector3d solution) const;

    Matrix3x3& matrix();
    Matrix3x3 matrix() const;

    Vector3d& vector();
    Vector3d vector() const;
};

std::ostream& operator << (std::ostream& strm, const SetOfEquations& set);
std::istream& operator >> (std::istream& strm, SetOfEquations& set);