#pragma once

#include "vector3d.hh"
#include <array>
#include <cmath>

#define SIZE 3
#define ERR_IDX_OUT_OF_RANGE -1
#define EPS 10e-10

using std::cerr;
using std::endl;
using std::array;


// Matrix of 3 vectors( Vector3d )
class Matrix3x3{
private:
    array<Vector3d, SIZE> _matrix;

public:
    Matrix3x3(Vector3d v1, Vector3d v2, Vector3d v3) : _matrix{v1,v2,v3}{};
    Matrix3x3() : Matrix3x3(Vector3d(), Vector3d(), Vector3d()) {};
    Matrix3x3(const Matrix3x3& matrix) : Matrix3x3(matrix[0], matrix[1], matrix[2]) {};

    Vector3d& operator [] (unsigned int idx);
    Vector3d operator [] (unsigned int idx) const;

    double& operator () (unsigned int row, unsigned int col);
    double operator () (unsigned int row, unsigned int col) const;
    
    Matrix3x3 transpose() const;
    void transpose_ip();  // transpose in place == change current _matrix to transposed one

    Matrix3x3 swapColumnWithVector(unsigned int column, const Vector3d& vector) const;
    Matrix3x3 swapRows(unsigned int row1, unsigned int row2) const;

    Vector3d diagonal(Vector3d& vector) const;
    double detGauss(Vector3d& vector) const;
    
    Matrix3x3 operator * (double d) const;
    Vector3d  operator * (const Vector3d& vector) const;
    Matrix3x3 operator * (const Matrix3x3& matrix) const;
    Matrix3x3 operator + (const Matrix3x3& matrix) const;
    Matrix3x3 operator - (const Matrix3x3& matrix) const;
};

std::ostream& operator << (std::ostream& strm, const Matrix3x3& matrix);
std::istream& operator >> (std::istream& strm, Matrix3x3& matrix);