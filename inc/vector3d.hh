#pragma once

#include <iostream>
#include <array>
#include <cmath>

#define SIZE 3
#define ERR_IDX_OUT_OF_RANGE -1 
#define ERR_DIV_BY_0 -2

using std::cerr;
using std::endl;
using std::array;

// Vector of 3 numbers (double)
class Vector3d{
private:
    array<double, SIZE> _vector;

public:
    Vector3d(double a, double b, double c) : _vector{a,b,c} {};
    Vector3d() : Vector3d(0,0,0) {};
    Vector3d(const Vector3d& vector) : Vector3d(vector[0], vector[1], vector[2]) {};

    double& operator [](unsigned int idx);
    double operator [](unsigned int idx) const;

    Vector3d operator * (double d) const;
    Vector3d& operator *= (double d);

    Vector3d operator + (const Vector3d& vector) const;
    Vector3d& operator += (const Vector3d& vector) ;

    Vector3d operator - (const Vector3d& vector) const;
    Vector3d& operator -= (const Vector3d& vector);

    double operator * (const Vector3d& vector) const;
    Vector3d operator / (double d) const;
    Vector3d vectorProduct(const Vector3d& vector) const;
    
    double length() const;
};

std::ostream& operator << (std::ostream& strm, const Vector3d& vector);
std::istream& operator >> (std::istream& strm, Vector3d& vector);