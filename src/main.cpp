/*
    Autor: Pawel Kowalczyk
    Indeks: 263491
    Projekt: Uklady rownan liniowych
    Data: 10.04.2022r
*/

#include <iostream>
#include "../inc/vector3d.hh"
#include "../inc/matrix3x3.hh"
#include "../inc/setOfEquations.hh"

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

int main(){

    SetOfEquations set;
    Vector3d solution;
     
    cin >> set;

    cout << endl << "Set: " << endl << set << endl;
    //set.matrix() = set.matrix().swapRows(0,1);
    //cout << set << endl;
    solution = set.solve();
    cout << "Solution: " << solution << endl; 
    
    // set.showPrettySolution(solution);

    // Vector3d inaccuracy = set.matrix() * solution - set.vector();
    //cout << endl << "Inaccuracy vector |Ax-b| = " << inaccuracy << endl;
    //cout << "Length of inaccuracy vector ||Ax-b|| = " << inaccuracy.length() << endl;
    
    return 0;
}