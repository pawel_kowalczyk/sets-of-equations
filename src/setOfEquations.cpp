#include "../inc/setOfEquations.hh"

// Solves set using Gaussian elimination and returns solution( Vector3d )
Vector3d SetOfEquations::solve() const {
    Vector3d temp = _absoluteTerm;
    Vector3d diagonal = _matrix.diagonal(temp);

    if( diagonal.length() == 0 ){
        cerr << "Main determinant is 0, can't solve set." << endl;
        exit(ERR_DET_0);
    }

    double z = temp[2] / diagonal[2];
    double y = temp[1] / diagonal[1];
    double x = temp[0] / diagonal[0];

    return Vector3d(x, y, z);
}

// Prints set with solution 
void SetOfEquations::showPrettySolution(Vector3d solution) const{
    cout << endl << "Set with solution: " << endl;
    cout <<  _matrix[0] << "| " << solution[0] << " |   | " << _absoluteTerm[0] << " |" << endl;
    cout <<  _matrix[1] << "| " << solution[1] << " | = | " << _absoluteTerm[1] << " |" << endl;
    cout <<  _matrix[2] << "| " << solution[2] << " |   | " << _absoluteTerm[2] << " |" << endl;
}

// Returns reference to matrix( Matrix3x3 ) of a set
Matrix3x3& SetOfEquations::matrix(){
    return _matrix;
}

// Returns matrix( Matrix3x3 ) of a set 
Matrix3x3 SetOfEquations::matrix() const{
    return _matrix;
}

// Returns reference to absolute term( Vector3d ) of a set
Vector3d& SetOfEquations::vector(){
    return _absoluteTerm;
}

// Returns absolute term( Vector3d ) of a set
Vector3d SetOfEquations::vector() const{
    return _absoluteTerm;
}

std::ostream& operator << (std::ostream& strm, const SetOfEquations& set){
    int i;
    strm << "| "; for(i=0; i<SIZE; i++) strm<<set.matrix()(0,i)<<" "; strm << "||x_1|   | " << set.vector()[0] << " |" << endl;
    strm << "| "; for(i=0; i<SIZE; i++) strm<<set.matrix()(1,i)<<" "; strm << "||x_2| = | " << set.vector()[1] << " |" << endl;
    strm << "| "; for(i=0; i<SIZE; i++) strm<<set.matrix()(2,i)<<" "; strm << "||x_3|   | " << set.vector()[2] << " |" << endl;

    return strm;
}

std::istream& operator >> (std::istream& strm, SetOfEquations& set){
    strm >> set.matrix() >> set.vector();
    set.matrix().transpose_ip();

    if(strm.fail()){
        cerr << "Could not read set of equations" << endl;
        exit(ERR_READING_SET);
    }

    return strm;
}