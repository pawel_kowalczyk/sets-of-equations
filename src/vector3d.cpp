#include "../inc/vector3d.hh"

// Returns a reference to field with given index
double& Vector3d::operator [](unsigned int idx){
    if(idx > 2){
        cerr << "In vector3d.cpp, double& operator [](int idx)" << endl;
        cerr << "Index ["<<idx<<"] out of range for vector3d" << endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _vector[idx];
}

// Returns a field with given index
double Vector3d::operator [](unsigned int idx) const{
    if(idx > 2){
        cerr << "In vector3d.cpp, double operator [](int idx)" << endl;
        cerr << "Index ["<<idx<<"] out of range for vector3d" << endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _vector[idx];
}

// Returns result( Vector3d ) of multiplying Vector3d with a number, order matters
Vector3d Vector3d::operator * (double d) const{
    return Vector3d(_vector[0]*d, _vector[1]*d, _vector[2]*d);
}

// Multiplies vector by given number, returns reference to Vector3d
Vector3d& Vector3d::operator *= (double d){
    return (*this) = (*this) * d;
}

// Returns result( Vector3d ) of adding two Vector3d
Vector3d Vector3d::operator + (const Vector3d& vector) const{
    return Vector3d(_vector[0]+vector[0], _vector[1]+vector[1], _vector[2]+vector[2]);
}

// Adds given Vector3d to this Vector3d, returns reference to Vector3d
Vector3d& Vector3d::operator += (const Vector3d& vector) {
    return (*this) = (*this) + vector;;
}

// Return result( Vector3d ) of subtracting two Vector3d
Vector3d Vector3d::operator - (const Vector3d& vector) const{
    return (*this) + (vector*(-1));
}

// Subtracts Vector3d with given Vector3d, returns reference to Vector3d
Vector3d& Vector3d::operator -= (const Vector3d& vector) {
    return (*this) = (*this) - vector;;
}

// Returns result( Vector3d ) of scalar multiplying two Vector3d
double Vector3d::operator * (const Vector3d& vector) const{
    return _vector[0]*vector[0] + _vector[1]*vector[1] + _vector[2]*vector[2];
}

// Returns result of dividing Vector3d and a number
Vector3d Vector3d::operator / (double d) const{
    if(d == 0.0f){
        cerr << "In vector3d.cpp, Vector3d operator / (double d) const" << endl;
        cerr << "Can't divide by 0" << endl;
        exit(ERR_DIV_BY_0);
    }

    return (*this) * (1/d);
}

// Returns vector product( Vector3d ) of two vectors( Vector3d )
Vector3d Vector3d::vectorProduct(const Vector3d& vector) const{
    return Vector3d(_vector[1]*vector[2] - _vector[2]*vector[1], _vector[2]*vector[0] - _vector[0]*vector[2], _vector[0]*vector[1] - _vector[1]*vector[0]);
}

// Returns length of a Vector3d
double Vector3d::length() const{
    return sqrt(pow(_vector[0], 2) + pow(_vector[1], 2) + pow(_vector[2], 2));
}

std::ostream& operator << (std::ostream& strm, const Vector3d& vector){
    strm << "| "<<vector[0] << " " << vector[1] << " " << vector[2] << " |";
    return strm;
}

std::istream& operator >> (std::istream& strm, Vector3d& vector){
    strm >> vector[0] >> vector[1] >> vector[2];
    return strm;
}