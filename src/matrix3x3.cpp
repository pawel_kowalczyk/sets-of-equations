#include "../inc/matrix3x3.hh"
#include "../inc/vector3d.hh"
#include <algorithm>

// Returns a reference to Vector3d with given index
Vector3d& Matrix3x3::operator [] (unsigned int idx){
    if(idx > 2){
        cerr << "In matrix3x3.cpp, Vector3d& operator []" << endl;
        cerr << "Index ["<<idx<<"] out of range for vector3d"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[idx];
}

// Returns a Vector3d with given index
Vector3d Matrix3x3::operator [] (unsigned int idx) const{
    if(idx > 2){
        cerr << "In matrix3x3.cpp, Vector3d operator []" << endl;
        cerr << "Index ["<<idx<<"] out of range for vector3d"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[idx];
}

// Returns a reference to number from matrix with given coords 
double& Matrix3x3::operator () (unsigned int row, unsigned int col){
    if(row > 2 || col > 2){
        cerr << "In matrix3x3.cpp, double& operator ()" << endl;
        cerr << "Index ("<< row <<","<< col <<") out of range for matrix3x3"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[row][col];
}

// Returns a number from matrix with given coords 
double Matrix3x3::operator () (unsigned int row, unsigned int col) const{
    if(row > 2 || col > 2){
        cerr << "In matrix3x3.cpp, double operator ()" << endl;
        cerr << "Index ("<< row <<","<< col <<") out of range for matrix3x3"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    return _matrix[row][col];
}

// Returns transposed Matrix3x3
Matrix3x3 Matrix3x3::transpose() const{
    return Matrix3x3 (  Vector3d(_matrix[0][0],_matrix[1][0],_matrix[2][0]), 
                        Vector3d(_matrix[0][1],_matrix[1][1],_matrix[2][1]), 
                        Vector3d(_matrix[0][2],_matrix[1][2],_matrix[2][2]));
}

// Transposes Matrix3x3
void Matrix3x3::transpose_ip(){
    (*this) = (*this).transpose();
}

// Returns a Matrix3x3 with swapped column with given Vector3d
Matrix3x3 Matrix3x3::swapColumnWithVector(unsigned int column, const Vector3d& vector) const{
    if(column > 2){
        cerr << "In matrix3x3.cpp, Matrix3x3 swapColumnWithVector(int column, const Vector3d& vector)" << endl;
        cerr << "Index ["<<column<<"] out of range for matrix3x3"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    Matrix3x3 temp = (*this).transpose();

    switch(column){
        case 0 :{temp = Matrix3x3(vector, temp[1], temp[2]);break;}
        case 1 :{temp = Matrix3x3(temp[0], vector, temp[2]);break;}
        case 2 :{temp = Matrix3x3(temp[0], temp[1], vector);break;}
    }

    return temp.transpose();
}

//Returns a Matrix3x3 with swapped rows of given indexes
Matrix3x3 Matrix3x3::swapRows(unsigned int row1, unsigned int row2) const{
    if(row1 > 2){
        cerr << "In matrix3x3.cpp, Matrix3x3 swapRows(int row1, int row2) const" << endl;
        cerr << "Index ["<< row1 <<"] out of range for matrix3x3"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }
    
    else if (row2 > 2){
        cerr << "In matrix3x3.cpp, Matrix3x3 swapRows(int row1, int row2) const" << endl;
        cerr << "Index ["<< row2 <<"] out of range for matrix3x3"<<endl;
        exit(ERR_IDX_OUT_OF_RANGE);
    }

    Matrix3x3 swappedMatrix = (*this);

    Vector3d tempVector = swappedMatrix[row1];
    swappedMatrix[row1] = swappedMatrix[row2];
    swappedMatrix[row2] = tempVector;

    return swappedMatrix;
}

// Returns a diagonal( Vector3d ) of Matrix3x3 and changes given Vector3d to solve set
Vector3d Matrix3x3::diagonal(Vector3d& vector) const{
    Matrix3x3 temp = (*this);
    int swapCounter;
    double k;

    for( int r=0; r<SIZE; r++ ){ 
        for( swapCounter=r+1; swapCounter<SIZE; swapCounter++ ){
            if( fabs(temp(r,r)) < EPS ){
                std::swap(vector[r], vector[swapCounter]);
                temp=temp.swapRows(r, swapCounter);
            }

            else
                break;
            }

            if( swapCounter == SIZE && r != 2){
                return Vector3d();
            }

        for( int i=r+1; i<SIZE; i++ ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            vector[i] -= vector[r]*k;
        }
    }

    for( int r=SIZE-1; r>=0; r-- ){ 
        if( fabs(temp(r,r)) < EPS )
            return Vector3d();

        for( int i=r-1; i>=0; i-- ){
            k = temp(i,r)/temp(r,r);
            temp[i] -= temp[r]*k;
            vector[i] -= vector[r]*k;
        }
    }

    return Vector3d(temp(0,0), temp(1,1), temp(2,2));
}

// Returns a determinant of Matrix3x3 and changes given Vector3d to solve set
double Matrix3x3::detGauss(Vector3d& vector) const{
    
    Vector3d diag = diagonal(vector);

    return diag[0]*diag[1]*diag[2];
}

// Returns a Matrix3x3 multiplied by a number( double )
Matrix3x3 Matrix3x3::operator * (double d) const{
    if(d == 0.0) return Matrix3x3();

    Matrix3x3 temp = (*this);
    for(int i=0; i<SIZE; i++)
        for(int j=0; j<SIZE; j++)
            temp(i,j) *= d;

    return temp;
}

// Returns result( Vector3d ) od multiplying Matrix3x3 with a Vector3d
Vector3d  Matrix3x3::operator * (const Vector3d& vector) const{
    if(vector.length() == 0.0) return Vector3d();

    return Vector3d(_matrix[0]*vector, _matrix[1]*vector, _matrix[2]*vector);
}

// Returns result( Matrix3x3 ) od multiplying Matrix3x3 with a Matrix3x3
Matrix3x3 Matrix3x3::operator * (const Matrix3x3& matrix) const{
    Matrix3x3 temp = matrix.transpose();

    return Matrix3x3((*this)*temp[0], (*this)*temp[1], (*this)*temp[2]).transpose();
}

// Returns result( Matrix3x3 ) od adding Matrix3x3 with a Matrix3x3
Matrix3x3 Matrix3x3::operator + (const Matrix3x3& matrix) const{
    Matrix3x3 temp = (*this);

    for(int i=0; i<SIZE; i++)
        for(int j=0; j<SIZE; j++)
            temp(i,j) += matrix(i,j);

    return temp;
}

// Returns result( Matrix3x3 ) od subtracting Matrix3x3 with a Matrix3x3
Matrix3x3 Matrix3x3::operator - (const Matrix3x3& matrix) const{
    return (*this) + (matrix*-1);
}

std::ostream& operator << (std::ostream& strm, const Matrix3x3& matrix){
    strm << "|" << matrix[0] << "|" << endl;
    strm << "|" << matrix[1] << "|" << endl;
    strm << "|" << matrix[2] << "|" << endl;
    return strm;
}

std::istream& operator >> (std::istream& strm, Matrix3x3& matrix){
    strm >> matrix[0] >> matrix[1] >> matrix[2];
    
    return strm;
}